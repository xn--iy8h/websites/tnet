#
# setup :
#  cd ..
#  git clone https://gitlab.com/xn--iy8h/websites/tnet.git && cd tnet
#  or locally
#  rm -rf tnet-test;
#  git clone /media/$USER/4TB/repos/hologit.gq/websites/xn--tnt-dsb.ml/.git tnet-test && cd tnet-test
#

uname -h
uid=$(id -u)
gitdir=$(git rev-parse --git-dir)
cat "$gitdir/config"
set
env

branch=public
qmgit=QmeQ4R8dYertFTt69vcdYHJMmaLMvXhoSiF2p5tzkma9Rz
ipns_key=QmXQsiSfPvSRDbFQihK2b6R2EqxXSjSYHpXAe3bfA3SRht

# default git from Antoine B. Volpone (peerid: QmQwi3Qtf9x4CWNLiii6XXE5DQesizd9cFS7eaJv3oF338)
qmgit=${qmgit:-QmekkpM5xEpbxGnbc9WXszVNwHDQgb2RgV7NismP3eQwMA}
git_url="https://gateway.pinata.cloud/ipfs/$qmgit"
curl -sL $git_url/info/refs
git remote -v
git show-ref
if ! git show-ref -q --heads $branch; then
 git branch $branch
fi
git checkout $branch
#git pull --ff-only $git_url
#git pull --rebase=merges $git_url $branch
git config user.email "builder-$uid@localhost"
git config name "$SITE_NAME@$DEPLOY_ID"
git pull --ff $git_url $branch
